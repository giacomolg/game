REQUIREMENTS
============

* Java JDK v >= 1.6 (<http://www.oracle.com/technetwork/java/javase/downloads/index.html>).
* The precompiled jar (in the download section) only requires Java JRE v >= 1.6.

INSTALLATION
============

     $ git clone https://bitbucket.org/giacomolg/game.git && cd game

COMPILE AND RUN
===============

**If you already have Gradle (<http://gradle.org>) substitute *./gradlew* with simply *gradle*.**

If you just want to run the game:

     $ ./gradlew desktop:run

To build a binary archive (.jar) and run it:

     $ ./gradlew desktop:dist && cd dist && java -jar *.jar
     