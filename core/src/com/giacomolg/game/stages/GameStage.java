package com.giacomolg.game.stages;

import box2dLight.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.giacomolg.game.actions.Emit;
import com.giacomolg.game.actions.Shoot;
import com.giacomolg.game.actors.*;
import com.giacomolg.game.actors.particles.TweenParticleEmitter;
import com.giacomolg.game.actors.player.Player;
import com.giacomolg.game.handlers.GameCollisionListener;
import com.giacomolg.game.resources.Assets;
import com.giacomolg.game.utils.B2DValues;

import java.util.Random;

import static com.giacomolg.game.Game.VECTOR_2;
import static com.giacomolg.game.utils.B2DValues.toB2D;

public class GameStage extends Stage {
    public static final Random RANDOM = new Random();
    public static ShapeRenderer SHAPE_RENDERER = new ShapeRenderer();

    private final boolean isDebug;
    private ShaderProgram shaderProgram;
    private World world;
    private Box2DDebugRenderer b2dRenderer;
    private OrthographicCamera b2dCam;
    private HUD hud;
    private RayHandler rayHandler;
    private PositionalLight godLight;
    private PointLight playerLight;
    private InputMultiplexer inputMultiplexer;
    private float u_time = -1;
    private Vector2 u_centre = new Vector2();

    public GameStage(Viewport viewport, boolean isDebug) {
        super(viewport);

        this.isDebug = isDebug;
        setDebugAll(isDebug);

        setupInputProcessor();

        createWorld();
        createLights();
        createActors();

        preload();

        // setCustomShader();
    }

    private void setCustomShader() {
        SpriteBatch sb = (SpriteBatch) getBatch();

        String vertexShader = Gdx.files.internal("shaders/vertex.glsl").readString();
        String fragmentShader = Gdx.files.internal("shaders/passthrough.fsh").readString();

        shaderProgram = new ShaderProgram(vertexShader, fragmentShader);

        if(!shaderProgram.isCompiled())
            throw new RuntimeException(shaderProgram.getLog());

        sb.setShader(shaderProgram);
    }

    private void preload() {
        Shoot.load();
        Emit.load();
        TweenParticleEmitter.load();

        Assets.load();
    }

    private void createWorld() {
        VECTOR_2.set(0, -9.81f);
        world = new World(VECTOR_2, true);
        b2dRenderer = new Box2DDebugRenderer();
        b2dCam = new OrthographicCamera();
        updateB2dCamSize(getCamera().viewportWidth, getCamera().viewportHeight);
        world.setContactListener(new GameCollisionListener());
    }

    private void createLights() {
        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(16 / 255f, 64 / 255f, 110 / 255f, 0.25f);
        rayHandler.setBlur(false);
        godLight = new ConeLight(rayHandler, 2110, new Color(255f, 255f, 255f, 0.75f), toB2D(getCamera().viewportHeight * 3),
                toB2D(getCamera().viewportWidth / 2), toB2D(getCamera().viewportHeight * 4), -90f, 90f);
        playerLight = new PointLight(rayHandler, 2110, new Color(255f, 255f, 255f, 0.75f), toB2D(150), 0, 0);
        godLight.setSoft(true);
        playerLight.setSoft(false);

        Light.setContactFilter(B2DValues.BULLET_BIT, (short) 0xff, (short) 0xff);
    }

    private void updateB2dCamSize(float width, float height) {
        b2dCam.setToOrtho(false, toB2D(width), toB2D(height));
    }

    public void setAsInputProcessor(InputProcessor inputProcessor) {
        inputMultiplexer.addProcessor(inputProcessor);
    }

    private void setupInputProcessor() {
        inputMultiplexer = new InputMultiplexer(this);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public boolean keyDown(int keyCode) {
        switch (keyCode) {
            case Input.Keys.ESCAPE:
                Gdx.app.exit();
                return true;
            default:
                return super.keyDown(keyCode);
        }
    }

    public void startRipple(float screenX, float screenY){
        u_centre.x = screenX;
        u_centre.y = screenY;
        u_time = 0;
    }

    private void createActors() {
        addActor(new Background());
        addActor(new Ground(world));

        Player player = new Player(world, playerLight);

        // TODO
        Random random = new Random();
        for (int i = 0; i < 200; i++) {
            VECTOR_2.set(5 + random.nextInt((int) getWidth() * 4), 10 + i * 40);
            addActor(new Gem(world, player, VECTOR_2));
        }

        addActor(player);

        hud = new HUD(getViewport());
        // addActor(hud);
    }

    public void resize(int width, int height) {
        getViewport().update(width, height);
        updateB2dCamSize(width, height);

//        shaderProgram.begin();
//        shaderProgram.setUniformf("u_resolution", width, height);
//        shaderProgram.end();
    }

    @Override
    public void draw() {
//        getBatch().begin();
//        getBatch().draw(tr, 0, 0, getWidth(), getHeight());
//        getBatch().end();

        super.draw();

        //rayHandler.render();

        // not affected by light from here on
        renderB2dWorld();

        hud.draw(getBatch(), 1);
    }

    @Override
    public void act() {
        float dt = Gdx.graphics.getDeltaTime();
        // IMPORTANT:
        // first the lights update
        // then world steps
        // and then the stage acts
        //updateLight();

        world.step(dt, 6, 6);

        super.act(dt);

        makeWorldCameraFollowStageCamera();

        hud.act(dt);
        //hud.setZIndex(99999);

//        if(u_time != -1f) {
//            if(u_time > 1f) {
//                u_time = -1.f;
//            }
//            else {
//                shaderProgram.begin();
//                shaderProgram.setUniformf("u_time", u_time / 1f);
//                if (u_time == 0) shaderProgram.setUniformf("u_centre", u_centre);
//                shaderProgram.end();
//
//                u_time += dt;
//            }
//        }
    }

    protected void updateLight() {
        godLight.setPosition(toB2D(getWidth() / 2), toB2D(getCamera().position.y + getHeight()));

        rayHandler.setCombinedMatrix(b2dCam.combined);
        rayHandler.update();
    }

    private void renderB2dWorld() {
        if (isDebug) b2dRenderer.render(world, b2dCam.combined);
    }


    private void makeWorldCameraFollowStageCamera() {
        Vector3 pos = getCamera().position;
        b2dCam.position.set(toB2D(pos.x), toB2D(pos.y), toB2D(pos.z));
        b2dCam.update();
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
        b2dRenderer.dispose();
        rayHandler.dispose();
    }
}
