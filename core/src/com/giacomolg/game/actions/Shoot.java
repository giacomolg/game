package com.giacomolg.game.actions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Pool;
import com.giacomolg.game.resources.Assets;
import com.giacomolg.game.actors.B2DActor;
import com.giacomolg.game.actors.Bullet;
import com.giacomolg.game.utils.PreloadingPool;

import static com.giacomolg.game.utils.Math.getPointOfRotation;

public class Shoot extends Action {

    private static Pool<Shoot> actionPool = new PreloadingPool<Shoot>(30, 30) {
        @Override
        protected Shoot newObject() {
            Gdx.app.error("Shoot", "new Shoot!");
            return new Shoot();
        }
    };

    private float delta;
    private Stage stage;
    private World world;
    private float y;
    private float x;
    private float angleRad;
    private boolean isConfigured = false;
    private SequenceAction recoil;

    public Shoot(){
        recoil = Actions.sequence(Actions.moveBy(-15f, 0, .25f, Interpolation.bounceOut),
                Actions.moveBy(15f, 0, .25f, Interpolation.bounceIn));
    }

    public Shoot configure(Actor actor, World world, float x, float y, float angleRad){

        setActor(actor);
        this.stage = actor.getStage();
        this.world = world;
        this.x = x;
        this.y = y;
        this.angleRad = angleRad;

        return this;
    }

    private void configureNow() {
        setPool(actionPool);

        Bullet bullet = getBullet(world, x, y, angleRad);

        stage.addActor(bullet);

        for(Action action : recoil.getActions()) {
            action.restart();
            action.setTarget(actor);
        }

        stage.addAction(Emit.create(bullet,
                x,
                y,
                world,
                2,
                10,
                .5f,
                .85f,
                Color.BLACK));

        recoil.setTarget(actor);

        B2DActor actor = (B2DActor) getActor();
        Vector2 velocity = actor.getBody().getLinearVelocity();
        Body body = actor.getBody();
        float mass = body.getMass();

        Vector2 xyImpulse = getPointOfRotation(angleRad, 15f);

        float cos = xyImpulse.x, sin = xyImpulse.y;
        float xImpulse = cos + (velocity.x > 0 && cos > 0 || velocity.x < 0 && cos < 0 ? mass * velocity.x : 0);
        float yImpulse = sin + (velocity.y > 0 && sin > 0 || velocity.y < 0 && sin < 0 ? mass * velocity.y : 0);

        bullet.getBody().applyLinearImpulse(
                xImpulse,
                yImpulse,
                0, 0,
                true);

        isConfigured = true;
    }

    private Bullet getBullet(World world, float x, float y, float angleRad) {
        Bullet bullet = (Bullet) target;
        if(bullet == null){
            bullet = new Bullet(world);
            target = bullet;
        }

        return bullet.reset(x, y, angleRad);
    }

    @Override
    public void setTarget(Actor target) {
        // prevent anybody to set the target actor since we do it on configure
    }

    @Override
    public void reset() {
        if(isConfigured) {
            isConfigured = false;
            delta = 0;
            getTarget().remove();
            recoil.restart();
        }
    }

    @Override
    public boolean act(float delta) {

        if(!isConfigured){
            configureNow();
            //TODO: make this configurable
            Assets.GUN_SHOT.play(0.5f);
        }

        recoil.act(delta);

        this.delta += delta;

        return !getTarget().isVisible() || this.delta > 4f;
    }

    public static Action create(Actor actor, World world, float x, float y, float angleRad){
        return actionPool.obtain().configure(actor, world, x, y, angleRad);
    }

    public static void load(){
        assert null != actionPool;
    }
}
