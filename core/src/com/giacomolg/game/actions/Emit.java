package com.giacomolg.game.actions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.utils.Pool;
import com.giacomolg.game.actors.B2DActor;
import com.giacomolg.game.actors.particles.PhysicsParticleEmitter;
import com.giacomolg.game.utils.B2DValues;
import com.giacomolg.game.utils.PreloadingPool;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.giacomolg.game.stages.GameStage.RANDOM;
import static java.lang.Math.max;

public class Emit extends Action {
    private static Pool<Emit> actionPool = new PreloadingPool<Emit>(30, 30) {
        @Override
        protected Emit newObject() {
            // Gdx.app.error("Emit", "new Emit!");
            return new Emit();
        }

        @Override
        public void free(Emit object) {
            super.free(object);

            // Gdx.app.error("Emit", "free: " + getFree());
        }
    };

    private Action action;
    private ScaleByAction scaleByAction;
    private ScaleToAction scaleToAction;
    private World world;
    private int amount;
    private float duration;
    private Color color;
    private boolean isConfigured = false;
    private float x, y;

    public Emit() {
        scaleByAction = getScaleByAction();
        scaleToAction = getScaleToAction();

        action = sequence(scaleByAction, scaleToAction);
    }

    public Emit configure(
            final Actor actor,
            float x,
            float y,
            final World world,
            final int minAmount,
            final int maxAmount,
            final float minDuration,
            final float maxDuration,
            final Color color) {

        this.actor = actor;
        this.x = x;
        this.y = y;
        this.world = world;
        this.amount = minAmount + Math.round(RANDOM.nextFloat() * (maxAmount - minAmount));
        this.duration = minDuration + RANDOM.nextFloat() * (maxDuration - minDuration);
        this.color = color;

        return this;
    }

    @Override
    public void setTarget(Actor target) {
        // prevent anybody to set the target actor since we do it on configure
    }

    private void configureNow() {
        setPool(actionPool);

        PhysicsParticleEmitter particleEmitter = getParticleEmitter(actor, world, amount, color);

        actor.getStage().addActor(particleEmitter);

        scaleByAction.setDuration(duration * 0.2f);
        scaleToAction.setDuration(duration * 0.8f);

        scaleByAction.setTarget(particleEmitter);
        scaleToAction.setTarget(particleEmitter);
        action.setTarget(particleEmitter);

        scaleToAction.setActor(particleEmitter);
        scaleByAction.setActor(particleEmitter);
        action.setActor(particleEmitter);

        isConfigured = true;
    }

    private PhysicsParticleEmitter getParticleEmitter(Actor actor, World world, int amount, Color color) {
        PhysicsParticleEmitter particleEmitter = (PhysicsParticleEmitter) target;

        if (particleEmitter == null) {
            particleEmitter = new PhysicsParticleEmitter();
            target = particleEmitter;
        }

        return particleEmitter.reset(world, x, y, amount, color);
    }

    private ScaleToAction getScaleToAction() {
        ScaleToAction scaleToAction = new ScaleToAction();
        scaleToAction.setScale(0, 0);

        return scaleToAction;
    }

    private ScaleByAction getScaleByAction() {
        ScaleByAction scaleByAction = new ScaleByAction();
        scaleByAction.setAmount(2f);

        return scaleByAction;
    }

    @Override
    public boolean act(float delta) {
        if (!isConfigured) {
            configureNow();
        }

        return action.act(delta);
    }

    @Override
    public void reset() {
        if(isConfigured) {
            isConfigured = false;
            scaleByAction.reset();
            scaleToAction.reset();
            action.restart();

            target.remove();
        }
    }

    public static Action create(B2DActor actor,
                                World world,
                                Contact contact,
                                Color color) {

        Vector2 contactPoint = B2DValues.fromB2D(contact.getWorldManifold().getPoints()[0]);
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        float relSpeed = fixtureA.getBody().getLinearVelocity().dst(fixtureB.getBody().getLinearVelocity());
        float factor = max(.25f, relSpeed / 8);

        return create(actor,
                contactPoint.x,
                contactPoint.y,
                world,
                Math.round(8 * factor),
                Math.round(24 * factor),
                factor,
                1.5f * factor,
                color);
    }

    public static Action create(final Actor actor,
                                final float x,
                                final float y,
                                final World world,
                                final int minAmount,
                                final int maxAmount,
                                final float minDuration,
                                final float maxDuration,
                                final Color color) {

        return actionPool.obtain()
                .configure(actor,
                        x,
                        y,
                        world,
                        minAmount,
                        maxAmount,
                        minDuration,
                        maxDuration,
                        color);
    }

    public static void load(){
        assert null != actionPool;
    }
}
