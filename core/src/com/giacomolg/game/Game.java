package com.giacomolg.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.giacomolg.game.stages.GameStage;

public class Game extends ApplicationAdapter {
    private static final boolean DEBUG = false;
    public static final Vector2 VECTOR_2 = new Vector2();
    public static double millisPerFrame;
    private GameStage gs;
    private long timeTaken;
    private int frames;

    @Override
    public void create() {
        gs = new GameStage(new ScreenViewport(), DEBUG);
    }

    @Override
    public void dispose() {
        gs.dispose();
    }

    @Override
    public void render() {
        Gdx.gl20.glClearColor(16 / 255f, 64 / 255f, 110 / 255f, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        long nanoTime = TimeUtils.nanoTime();

        gs.act();
        gs.draw();

        timeTaken += TimeUtils.nanoTime() - nanoTime;
        frames++;

        if (60 * 5 <= frames) {
            millisPerFrame = timeTaken / (60 * 5) / 1000000;
            timeTaken = 0;
            frames = 0;
        }

    }

    @Override
    public void resize(int width, int height) {
        gs.resize(width, height);
    }
}
