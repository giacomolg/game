package com.giacomolg.game.handlers;

import com.badlogic.gdx.physics.box2d.*;
import com.giacomolg.game.actors.CollidingActor;

public class GameCollisionListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Object a = contact.getFixtureA().getUserData();
        Object b = contact.getFixtureB().getUserData();

        if(a instanceof CollidingActor){
            ((CollidingActor)a).beginContact(contact);
        }

        if(b instanceof CollidingActor){
            ((CollidingActor)b).beginContact(contact);
        }
    }

    @Override
    public void endContact(Contact contact) {
        Object a = contact.getFixtureA().getUserData();
        Object b = contact.getFixtureB().getUserData();

        if(a instanceof CollidingActor){
            ((CollidingActor)a).endContact(contact);
        }

        if(b instanceof CollidingActor){
            ((CollidingActor)b).endContact(contact);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Object a = contact.getFixtureA().getUserData();
        Object b = contact.getFixtureB().getUserData();

        if(a instanceof CollidingActor){
            ((CollidingActor)a).preSolve(contact, oldManifold);
        }

        if(b instanceof CollidingActor){
            ((CollidingActor)b).preSolve(contact, oldManifold);
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Object a = contact.getFixtureA().getUserData();
        Object b = contact.getFixtureB().getUserData();

        if(a instanceof CollidingActor){
            ((CollidingActor) a).postSolve(contact, impulse);
        }

        if(b instanceof CollidingActor){
            ((CollidingActor) b).postSolve(contact, impulse);
        }
    }
}
