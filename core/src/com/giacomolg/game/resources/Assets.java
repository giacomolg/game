package com.giacomolg.game.resources;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

    public static Sound GUN_SHOT = Gdx.audio.newSound(Gdx.files.internal("sounds/M4A1_Single-Kibblesbob-8540445.mp3"));
    public static Sound BLOOD_SPLATTER = Gdx.audio.newSound(Gdx.files.internal("sounds/blood_splatter.mp3"));

    public static TextureRegion BULLET_SPRITE = new TextureRegion(new Texture("bullet2.png"));


    public static void load(){
        assert BULLET_SPRITE != null;
    }
}
