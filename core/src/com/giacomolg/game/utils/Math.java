package com.giacomolg.game.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.giacomolg.game.Game;

import static java.lang.Math.*;

public class Math {

    public static Vector2 getPointOfRotation(float angleRadians, float r){
        return getPointOfRotation(angleRadians, r, Game.VECTOR_2);
    }

    public static Vector2 getPointOfRotation(float angleRadians, float r, Vector2 vec2) {
        double x1 = com.badlogic.gdx.math.MathUtils.cos(angleRadians) * r;
        double y1 = com.badlogic.gdx.math.MathUtils.sin(angleRadians) * r;

        vec2.set((float) x1, (float) y1);

        return vec2;
    }

    public static void moveAtSpeedX(Body body, float delta, float speed) {
        Vector2 linearVelocity = body.getLinearVelocity();
        float velChange = speed - linearVelocity.x;
        float force = body.getMass() * velChange / delta;

        body.applyForceToCenter(force, 0, true);
    }

    public static void moveAtSpeedY(Body body, float delta, float speed) {
        Vector2 linearVelocity = body.getLinearVelocity();
        float velChange = speed - linearVelocity.y;
        float force = body.getMass() * velChange / delta;

        body.applyForceToCenter(0, force, true);
    }

    public static void applyForceY(Body body, float delta, float maxSpeed) {
        Vector2 linearVelocity = body.getLinearVelocity();
        float velChange = maxSpeed - linearVelocity.y;
        if(velChange > 0) {
            float force = body.getMass() * velChange / delta;

            body.applyForceToCenter(0, force, true);
        }
    }
}
