package com.giacomolg.game.utils;

import com.badlogic.gdx.math.Vector2;

public abstract class B2DValues {
    public static final short PLAYER_BITS = 1;
    public static final short GROUND_BIT = PLAYER_BITS << 1;
    public static final short BULLET_BIT = GROUND_BIT << 1;
    public static short EXPLOSION_BIT = BULLET_BIT << 1;
    public static final float BBD = 100;

    public static Vector2 toB2D(Vector2 v){ return v.scl(1/BBD); }
    public static float toB2D(float v){ return v / BBD; }
    public static float[] toB2D(float[] vs){
        float[] b2dVs = new float[vs.length];
        for (int i = 0; i < vs.length; i++) {
            b2dVs[i] = vs[i] / BBD;
        }
        return b2dVs;
    }

    public static Vector2 fromB2D(Vector2 v){ return v.scl(BBD); }
    public static float fromB2D(float v){ return v * BBD; }
    public static float[] fromB2D(float[] vs){
        float[] fromB2dVs = new float[vs.length];
        for (int i = 0; i < vs.length; i++) {
            fromB2dVs[i] = vs[i] * BBD;
        }
        return fromB2dVs;
    }
}
