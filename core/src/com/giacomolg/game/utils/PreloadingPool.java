package com.giacomolg.game.utils;

import com.badlogic.gdx.utils.Pool;

public abstract class PreloadingPool<T> extends Pool<T> {

    public PreloadingPool(int initCapacity, int max) {
        super(initCapacity, max);

        for (int i = 0; i < initCapacity; i++) {
            free(newObject());
        }
    }
}
