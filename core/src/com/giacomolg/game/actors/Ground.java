package com.giacomolg.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.giacomolg.game.utils.B2DValues;

import static com.giacomolg.game.stages.GameStage.SHAPE_RENDERER;
import static com.giacomolg.game.utils.B2DValues.toB2D;

public class Ground extends B2DActor {

    public Ground(World world) {
        super(world);

        int width = Gdx.graphics.getWidth() << 7;
        setBounds(-width, 0, width << 1, 5);
        FixtureDef fdef = new FixtureDef();
        fdef.friction = 1;
        fdef.filter.categoryBits = B2DValues.GROUND_BIT;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(toB2D(getScaledWidth() / 2), toB2D(getScaledHeight()) / 2);
        initB2D(BodyDef.BodyType.StaticBody, shape, fdef);
        shape.dispose();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        SHAPE_RENDERER.setTransformMatrix(batch.getTransformMatrix());
        SHAPE_RENDERER.setProjectionMatrix(batch.getProjectionMatrix());

        SHAPE_RENDERER.begin(ShapeRenderer.ShapeType.Filled);
        SHAPE_RENDERER.setColor(Color.OLIVE);
        SHAPE_RENDERER.rect(getX(), getY(), getWidth(), getHeight() + 3);
        SHAPE_RENDERER.end();

        batch.begin();
    }

}
