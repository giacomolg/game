package com.giacomolg.game.actors.player;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

enum AnimState {
    IDLE,
    MOVING_LEFT,
    MOVING_RIGHT,
    JUMPING;

    private boolean flip = false;
    private TextureRegion frame;
    private short direction = 1;

    public void setDirectionAndFrame(short direction, TextureRegion frame) {
        setDirection(direction);
        setFrame(frame);
    }

    public void setFrame(TextureRegion frame) {
        if (flip && !frame.isFlipX()
                || !flip && frame.isFlipX()){
            frame.flip(true, false);
        }

        this.frame = frame;
    }

    public TextureRegion getFrame() {
        return frame;
    }

    private void setDirection(short direction) {
        if(direction != 0)
            flip = direction == -1;
        this.direction = direction;
    }
}
