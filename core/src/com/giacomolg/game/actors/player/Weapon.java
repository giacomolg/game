package com.giacomolg.game.actors.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.giacomolg.game.actions.Shoot;
import com.giacomolg.game.actors.Follower;
import com.giacomolg.game.actors.particles.TweenParticleEmitter;
import com.giacomolg.game.stages.GameStage;

import static com.giacomolg.game.stages.GameStage.SHAPE_RENDERER;
import static com.giacomolg.game.utils.Math.getPointOfRotation;
import static java.lang.Math.atan2;

public class Weapon extends Actor implements InputProcessor {
    private static final Color COLOR = new Color(1, 106f / 255f, 0, 1);
//    private static final Color SMOKE_COLOR = Color.PURPLE;
    private final Player player;
    private final float rateOfFire;
    private final float range;
    private final Mode mode;
    private float armRotationRadians;
    private float armXOffset;
    private float timeSinceLastShooting = Float.MIN_VALUE;
    private boolean shooting;
    private Actor tip;
    private Actor smoke;

    public enum Mode {
        AUTOMATIC,
        MANUAL
    }

    public Weapon(Player player,
                  float x, float y,
                  float w, float h,
                  float rateOfFire, float range,
                  Mode mode) {
        this.player = player;
        this.rateOfFire = rateOfFire;
        this.range = range;
        this.mode = mode;

        setBounds(x, y, w, h);
        setColor(COLOR);

        GameStage stage = (GameStage) player.getStage();
        stage.setAsInputProcessor(this);
        setStage(stage);

        tip = new Tip();
        addSmoke();
    }

    private void addSmoke() {
        smoke = TweenParticleEmitter.emitSmoke(
                15,
                1.2f,
                tip.getX(),
                tip.getY(),
                Float.MAX_VALUE,
                Color.PURPLE, null);

        getStage().addActor(Follower.makeFollowerOf(smoke, tip));
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        tip.act(delta);

        updateArmXOffset();
        updateShooting(delta);
    }

    private void updateShooting(float delta) {
        float tmp = timeSinceLastShooting;
        timeSinceLastShooting += delta;

        if (isShooting()) {
            //Gdx.app.error("Weapon", timeSinceLastShooting+"");
            shoot(tmp == Float.MIN_VALUE);

            shooting = false;
        }

    }

    private boolean isShooting(){
        switch (mode){
            case AUTOMATIC:
                return Gdx.input.isButtonPressed(Input.Buttons.LEFT);
            case MANUAL:
                return shooting;
        }

        return false;
    }

    private void updateArmXOffset() {
        float xOffset = 15;
        armXOffset = player.getDirection() == 1 ? xOffset : player.getWidth() - xOffset;
    }

    private void updateArmRotation(float mouseX, int mouseY) {
        Vector3 camPos = getStage().getCamera().position;
        // set y coordinate as y-down
        // and add camera offset to mouse position
        float pointerY = Gdx.graphics.getHeight() - mouseY + camPos.y - Gdx.graphics.getHeight() / 2;
        float pointerX = mouseX + camPos.x - Gdx.graphics.getWidth() / 2;

        float opposite = pointerY - getArmY();
        float adjacent = pointerX - getArmX();

        armRotationRadians = (float) atan2(opposite, adjacent);
    }

    private void shoot(boolean firstTime) {
        if (firstTime || timeSinceLastShooting >= rateOfFire) {

            Vector2 pointOfRot = getPointOfRotation(armRotationRadians, getWidth() + 10);

            player.addAction(Shoot.create(player,
                    player.getWorld(),
                    getArmX() + pointOfRot.x,
                    getArmY() + pointOfRot.y,
                    armRotationRadians));

            timeSinceLastShooting = 0;

            smoke.addAction(Actions.color(smoke.getColor(), rateOfFire - timeSinceLastShooting - 0.05f));
            smoke.setColor(Color.WHITE);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        SHAPE_RENDERER.setTransformMatrix(batch.getTransformMatrix());
        SHAPE_RENDERER.setProjectionMatrix(batch.getProjectionMatrix());

        SHAPE_RENDERER.begin(ShapeRenderer.ShapeType.Filled);
        SHAPE_RENDERER.setColor(getColor());
        float x = getArmX();
        float y = getArmY();

        SHAPE_RENDERER.rect(x, y,
                0, getHeight() / 2, /*origin for rotation*/
                getWidth(), getHeight(),
                getScaleX(), getScaleY(), /*scale*/
                MathUtils.radiansToDegrees * armRotationRadians /*rotation*/
        );
        SHAPE_RENDERER.end();

        batch.begin();
    }

    private float getArmY() {
        return getY();
    }

    private float getArmX() {
        return getX() + armXOffset;
    }

    @Override
    public float getX() {
        return player.getX() + super.getX();
    }

    @Override
    public float getY() {
        return player.getY() + super.getY();
    }

    private class Tip extends Actor {

        Tip() {
            act(0);
        }

        @Override
        public void act(float delta) {

            Vector2 pointOfRot = getPointOfRotation(armRotationRadians, Weapon.this.getWidth() + 10);

            this.setX(getArmX() + pointOfRot.x);
            this.setY(getArmY() + pointOfRot.y);
        }

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(Input.Buttons.LEFT == button)
            shooting = true;
        // propagate event
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        updateArmRotation(screenX, screenY);
        // propagate event
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        updateArmRotation(screenX, screenY);
        // propagate event
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
