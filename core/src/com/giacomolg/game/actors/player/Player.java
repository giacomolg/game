package com.giacomolg.game.actors.player;

import box2dLight.PointLight;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.giacomolg.game.actions.Emit;
import com.giacomolg.game.actors.B2DActor;
import com.giacomolg.game.actors.Ground;
import com.giacomolg.game.resources.Assets;
import com.giacomolg.game.stages.GameStage;
import com.giacomolg.game.utils.B2DValues;

import static com.giacomolg.game.utils.B2DValues.toB2D;
import static java.lang.Math.max;

public class Player extends B2DActor implements InputProcessor {
    private static final float PLAYER_DENSITY = 8;
    private static final int JUMPS_NUMBER = 3;

    static {
        // load bullet texture
        assert null != Assets.BULLET_SPRITE;
    }

    private final PointLight light;
    private Texture texture;
    private Animation idleAnimation;
    private int groundContact;
    private AnimState animState = AnimState.IDLE;
    private float animTime;
    private short direction = 1;
    private int jumpsRemaining = JUMPS_NUMBER;
    private boolean jumping;
    private Weapon weapon;

    public Player(World world, PointLight light) {
        super(world);
        this.light = light;

        setWidth(50);
        setHeight(60);

        setBounds(Gdx.graphics.getWidth() / 2 - getWidth() / 2,
                Gdx.graphics.getHeight() / 6,
                getWidth(), getHeight());

        setOrigin(getX() + getWidth() / 2, getY() + getHeight() / 2);

        initB2D(BodyDef.BodyType.DynamicBody);

        createAnimations();
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        GameStage gameStage = (GameStage) getStage();

        if (gameStage != null) {
            gameStage.setAsInputProcessor(this);
            addWeapon();
        }
    }

    private void addWeapon() {
        weapon = new Weapon(this, 0, getHeight() * .55f, 32f, 8f, .3f, 1000f, Weapon.Mode.AUTOMATIC);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        Vector2 linearVelocity = body.getLinearVelocity();

        handleMovement(delta, linearVelocity);

        updateCamera();

        updateAnimation(delta);

        weapon.act(delta);

        updateLight();
    }

    private void handleMovement(float delta, Vector2 linearVelocity) {
        boolean leftPressed = Gdx.input.isKeyPressed(Input.Keys.A);
        boolean rightPressed = Gdx.input.isKeyPressed(Input.Keys.D);
        boolean isOnGround = groundContact > 0;

        if (leftPressed) {
            moveLeft(isOnGround, delta);
        }
        if (rightPressed) {
            moveRight(isOnGround, delta);
        }

        if (isOnGround && !(leftPressed || rightPressed)) {
            body.setLinearVelocity(linearVelocity.scl(0.5f));
        }

        if (jumping && jumpsRemaining > 0) {
            jump(delta);
            jumpsRemaining--;
        }

        jumping = false;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(animState.getFrame(), getX(), getY(), getScaledWidth(), getScaledHeight());

        weapon.draw(batch, parentAlpha);
    }

    @Override
    protected void initB2D(BodyDef.BodyType type) {
        BodyDef bdef = new BodyDef();
        bdef.type = type;
        bdef.position.set(toB2D(getX() + getScaledWidth() / 2), toB2D(getY() + getScaledHeight() / 2));
        body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        fdef.density = PLAYER_DENSITY;
        fdef.filter.categoryBits = B2DValues.PLAYER_BITS;

        PolygonShape shape = new PolygonShape();
        shape.set(new float[]{toB2D(-getScaledWidth() / 2), toB2D(-getScaledHeight() / 2),
                toB2D(getScaledWidth() / 2), toB2D(-getScaledHeight() / 2),
                toB2D(getScaledWidth() / 3), toB2D(getScaledHeight() / 2),
                toB2D(-getScaledWidth() / 3), toB2D(getScaledHeight() / 2),
        });
        fdef.shape = shape;

        body.createFixture(fdef)
                .setUserData(this);

        shape.dispose();
    }

    public short getDirection() {
        return direction;
    }

    private void createAnimations() {
        texture = new Texture("player.png");
        TextureRegion[] textureRegions = new TextureRegion[]{
                new TextureRegion(texture, 0, 0, 25, 30),
                //new TextureRegion(texture, 25, 0, 25, 30),
        };
        idleAnimation = new Animation(1 / 3f, new Array<TextureRegion>(textureRegions), Animation.PlayMode.LOOP_PINGPONG);
    }

    private void updateCamera() {
        Camera camera = getStage().getCamera();
        Vector3 camPos = camera.position;
        camera.position.set(
                camPos.x + (getX() - camPos.x) * 0.125f,
                max(camPos.y + (getY() - camPos.y) * 0.85f, Gdx.graphics.getHeight() >> 1),
                0);
    }

    private void updateLight() {
        light.setPosition(toB2D(getX() + getWidth() / 2), toB2D(getY() + getHeight() / 2));
    }

    private void updateAnimation(float delta) {
        Vector2 linearVelocity = getBody().getLinearVelocity();
        if (linearVelocity.isZero()) {
            animTime += delta;

            animState.setFrame(idleAnimation.getKeyFrame(animTime));
        } else {
            animState.setDirectionAndFrame(direction, idleAnimation.getKeyFrame(0));
            animTime = 0;
        }
    }

    private void jump(float delta) {
        body.setLinearVelocity(body.getLinearVelocity().x, 0);
        body.applyForceToCenter(0, body.getMass() * 5 / delta, true);
    }

    private void moveRight(boolean isOnGround, float delta) {
        moveHorizontally(5 * (isOnGround ? 1 : 0.75f), delta);
        direction = 1;
    }

    private void moveLeft(boolean isOnGround, float delta) {
        moveHorizontally(-5 * (isOnGround ? 1 : 0.75f), delta);
        direction = -1;
    }

    private void moveHorizontally(float velocityX, float delta) {
        com.giacomolg.game.utils.Math.moveAtSpeedX(body, delta, velocityX);
    }

    private void doGroundParticleEffect(Contact contact) {
        addAction(Emit.create(this, getWorld(), contact, Color.GREEN));
        ((GameStage) getStage()).startRipple(getX(), getY());
    }

    private boolean isWithGround(Contact contact) {
        Object userData = getCollidingFixture(contact).getUserData();
        return userData != null && Ground.class == userData.getClass();
    }

    @Override
    public void beginContact(Contact contact) {
        if (isWithGround(contact)) {
            groundContact++;
            if (groundContact > 0) {
                jumpsRemaining = JUMPS_NUMBER;
                doGroundParticleEffect(contact);
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        if (isWithGround(contact))
            groundContact--;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
            case Input.Keys.SPACE:
                jumping = true;
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
