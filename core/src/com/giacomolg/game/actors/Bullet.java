package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.giacomolg.game.actors.particles.TweenParticleEmitter;
import com.giacomolg.game.actors.player.Player;
import com.giacomolg.game.resources.Assets;
import com.giacomolg.game.utils.*;

import static com.giacomolg.game.utils.B2DValues.toB2D;
import static com.giacomolg.game.utils.Math.getPointOfRotation;

public class Bullet extends B2DActor {
    private static FixtureDef fdef = new FixtureDef();
    private static PolygonShape shape;

    static {
        fdef.isSensor = true;
        fdef.filter.maskBits = B2DValues.PLAYER_BITS | B2DValues.GROUND_BIT;
        fdef.filter.categoryBits = B2DValues.BULLET_BIT;
        fdef.density = 0;
        shape = new PolygonShape();
    }

    private short powerLeftToGive = 1;

    public Bullet(World world) {
        super(world);
        setWidth(16);
        setHeight(10);
        setScale(4, 4);

        setOrigin(getWidth() / 2, getHeight() / 2);

        shape.setAsBox(toB2D(getScaledWidth() / 2),
                toB2D(getScaledHeight() / 2));
    }

    public Bullet reset(float x, float y, float angleRad) {
        setPosition(x, y);
        setVisible(true);

        initB2D(BodyDef.BodyType.DynamicBody, shape, fdef);

        body.setTransform(toB2D(x), toB2D(y), angleRad);
        body.setGravityScale(0);

        powerLeftToGive = 1;

        // the first time the actor is added (during an act loop)
        // the draw function is called once without the preceding act
        act(0);

        return this;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Assets.BULLET_SPRITE,
                getX(),
                getY(),
                getOriginX(),
                getOriginY(),
                getWidth(),
                getHeight(),
                getScaleX(),
                getScaleY(),
                getRotation());
    }

    public boolean hasPowerLeft() {
        if (powerLeftToGive <= 0)
            return false;

        powerLeftToGive--;

        return true;
    }

    private void explode() {
        Vector2 pointOfRotation = getPointOfRotation(MathUtils.degreesToRadians * getRotation(), getHeight());
        getStage().addActor(TweenParticleEmitter.emitExplosion(
                100,
                .5f,
                getX() + pointOfRotation.x,
                getY() + pointOfRotation.y,
                Float.MIN_VALUE,
                Color.YELLOW,
                null));
    }

    @Override
    public void beginContact(Contact contact) {
        Object actor = getCollidingFixture(contact).getUserData();
        if (!(actor instanceof Player)) {
            explode();
            remove();
        }
    }
}