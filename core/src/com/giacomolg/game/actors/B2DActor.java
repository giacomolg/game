package com.giacomolg.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;

import static com.giacomolg.game.utils.B2DValues.fromB2D;
import static com.giacomolg.game.utils.B2DValues.toB2D;

public abstract class B2DActor extends Actor implements CollidingActor {
    protected static final int REMOVAL_QUEUED = 0;
    protected static BodyDef bdef = new BodyDef();
    protected final World world;
    protected Body body;
    private boolean[] queuedEvents = new boolean[1];
    private int contacts;

    public B2DActor(World world) {
        this.world = world;
    }

    @Override
    public void act(float delta) {
        processQueue();

        Vector2 newPos = body.getPosition();
        setX(fromB2D(newPos.x) - getWidth() / 2);
        setY(fromB2D(newPos.y) - getHeight() / 2);

        setRotation((float) Math.toDegrees(body.getAngle()));

        super.act(delta);
    }

    @Override
    public boolean remove() {
        if (world.isLocked()) {
            queueRemoval();
            return false;
        }

        boolean removed = super.remove();
        if (removed) {
            // Gdx.app.error("B2DActor", "destroying " + this.getClass() + ", id = "  + body.hashCode());
            world.destroyBody(body);
        }

        return removed;
    }

    public World getWorld() {
        return world;
    }

    public Body getBody() {
        return body;
    }

    protected float getScaledHeight() {
        return getHeight() * getScaleY();
    }

    protected float getScaledWidth() {
        return getWidth() * getScaleX();
    }

    protected void initB2D(BodyDef.BodyType type) {
        initB2D(type, new FixtureDef());
    }

    protected void initB2D(BodyDef.BodyType type, FixtureDef fdef) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(toB2D(getScaledWidth() / 2), toB2D(getScaledHeight()) / 2);

        initB2D(type, shape, fdef);

        shape.dispose();
    }

    protected void initB2D(BodyDef.BodyType type, Shape shape, FixtureDef fdef) {
        bdef.type = type;
        bdef.position.set(toB2D(getX() + getScaledWidth() / 2),
                toB2D(getY() + getScaledHeight() / 2));

        body = world.createBody(bdef);

        fdef.shape = shape;

        body.createFixture(fdef)
                .setUserData(this);
    }

    protected Fixture getCollidingFixture(Contact contact) {
        Object a = contact.getFixtureA().getUserData();

        if (a == this) {
            return contact.getFixtureB();
        } else {
            return contact.getFixtureA();
        }
    }

    private void processQueue() {
        if (queuedEvents[REMOVAL_QUEUED]) {
            remove();
            queuedEvents[REMOVAL_QUEUED] = false;

        }
    }

    private void queueRemoval() {
        queuedEvents[REMOVAL_QUEUED] = true;
    }


    @Override
    public void beginContact(Contact contact) {  }

    @Override
    public void endContact(Contact contact) {  }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {  }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {  }
}
