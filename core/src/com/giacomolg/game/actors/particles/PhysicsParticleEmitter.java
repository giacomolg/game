package com.giacomolg.game.actors.particles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.giacomolg.game.utils.B2DValues;

import static com.giacomolg.game.stages.GameStage.RANDOM;
import static com.giacomolg.game.stages.GameStage.SHAPE_RENDERER;
import static com.giacomolg.game.utils.B2DValues.fromB2D;
import static com.giacomolg.game.utils.B2DValues.toB2D;

public class PhysicsParticleEmitter extends Actor {
    private Array<Body> particles;
    private BodyDef bodyDef;
    private FixtureDef fdef;
    private Shape shape;

    public PhysicsParticleEmitter() {
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        fdef = new FixtureDef();
        fdef.restitution = 0.5f;
        fdef.friction = 0.05f;
        fdef.isSensor = false;
        fdef.filter.categoryBits = B2DValues.EXPLOSION_BIT;
        fdef.filter.maskBits = (short) (B2DValues.GROUND_BIT | B2DValues.EXPLOSION_BIT);
        shape = new CircleShape();
    }

    public PhysicsParticleEmitter reset(World world, float x, float y, int particleAmount, Color color) {
        setBounds(x, y, 10, 10);
        addParticle(particleAmount, world);
        setColor(color);
        setScale(1, 1);

        // the first time the actor is added (during an act loop)
        // the draw function is called once without the preceding act
        act(0);

        return this;
    }

    @Override
    public boolean remove() {
        for (Body particle : particles) {
            particle.getWorld().destroyBody(particle);
        }

        particles.clear();

        return super.remove();
    }

    private void addParticle(int amount, World world) {
        bodyDef.position.set(toB2D(getX() + getWidth() / 2),
                toB2D(getY() + getHeight() / 2));

        if (particles == null) {
            particles = new Array<Body>(false, amount);
        }

        for (int i = 0; i < amount; i++) {
            Body particle = world.createBody(bodyDef);

            float hs = toB2D(2 + RANDOM.nextFloat() * 1.5f);
            shape.setRadius(hs);
            fdef.shape = shape;
            particle.createFixture(fdef);
            particle.setAwake(false);
            particle.applyForceToCenter((RANDOM.nextBoolean() ? 1 : -1) * 100 + RANDOM.nextFloat() * (45 - 15),
                    (RANDOM.nextBoolean() ? 1 : -1) * 200 + RANDOM.nextFloat() * (55 - 25), true);
            particles.add(particle);
        }
    }

    @Override
    public void act(float delta) {
        for (Body particle : particles) {
            Fixture fixture = particle.getFixtureList().first();
            Shape shape = fixture.getShape();
            Float originalRadius = (Float) fixture.getUserData();

            float radius = shape.getRadius();
            if (originalRadius == null) {
                originalRadius = radius;
                fixture.setUserData(originalRadius);
            }

            shape.setRadius(originalRadius * getScaleX());
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        drawParticles(batch);

        batch.begin();
    }

    private void drawParticles(Batch batch) {
        //If you want transparency
        // Gdx.gl.glEnable(GL20.GL_BLEND);
        SHAPE_RENDERER.setTransformMatrix(batch.getTransformMatrix());
        SHAPE_RENDERER.setTransformMatrix(batch.getTransformMatrix());
        SHAPE_RENDERER.begin(ShapeRenderer.ShapeType.Filled);
        SHAPE_RENDERER.setColor(getColor());

        for (Body particle : particles) {
            Fixture fixture = particle.getFixtureList().first();
            CircleShape shape = (CircleShape) fixture.getShape();

            float w = fromB2D(shape.getRadius()) * getScaleX();
            float h = w;

            Vector2 particlePosition = particle.getPosition();
            float x = fromB2D(particlePosition.x);
            float y = fromB2D(particlePosition.y);

            if ((particle.hashCode() & 1) == 0) SHAPE_RENDERER.rect(x - w / 2, y - h / 2, w, h);
            else SHAPE_RENDERER.ellipse(x - w / 2, y - h / 2, w, h);
        }

        SHAPE_RENDERER.end();
        // Gdx.gl.glDisable(GL20.GL_BLEND);
    }
}
