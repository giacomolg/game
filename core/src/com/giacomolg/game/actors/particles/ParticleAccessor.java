package com.giacomolg.game.actors.particles;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class ParticleAccessor implements TweenAccessor<Sprite> {
    public static final int X = 1;
    public static final int Y = 2;
    public static final int XY = 3;
    public static final int ALPHA = 4;
    public static final int SCALE = 5;

    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case X: returnValues[0] = target.getX(); return 1;
            case Y: returnValues[0] = target.getY(); return 1;
            case XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2;
            case ALPHA:
               returnValues[0] = target.getColor().a; return 1;
            case SCALE:
                returnValues[0] = target.getScaleX();
                returnValues[1] = target.getScaleY();
                return 2;
            default: assert false; return 0;
        }
    }

    public void setValues(Sprite target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case X: target.setX(newValues[0]); break;
            case Y: target.setY(newValues[1]); break;
            case XY:
                target.setX(newValues[0]);
                target.setY(newValues[1]);
                break;
            case ALPHA:
                target.setAlpha(newValues[0]);
                break;
            case SCALE:
                target.setScale(newValues[0], newValues[1]);
                break;
            default: assert false; break;
        }
    }
}
