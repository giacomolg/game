package com.giacomolg.game.actors.particles;

import aurelienribon.tweenengine.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.giacomolg.game.actors.Translatable;
import com.giacomolg.game.utils.PreloadingPool;

import java.util.List;

import static com.badlogic.gdx.math.MathUtils.PI;
import static com.giacomolg.game.stages.GameStage.RANDOM;
import static com.giacomolg.game.utils.Math.getPointOfRotation;

public class TweenParticleEmitter extends Actor implements Translatable {

    public enum Type {
        EXPLOSION,
        SMOKE
    }

    private static Pool<TweenParticleEmitter> actorPool = new PreloadingPool<TweenParticleEmitter>(100, 200) {
        @Override
        protected TweenParticleEmitter newObject() {
            // Gdx.app.error("TweenParticleEmitter", "new TweenParticleEmitter!");
            return new TweenParticleEmitter();
        }

        @Override
        public TweenParticleEmitter obtain() {
            // Gdx.app.error("TweenParticleEmitter", "free: " + getFree());
            return super.obtain();
        }

        @Override
        public void free(TweenParticleEmitter object) {
            super.free(object);
            // Gdx.app.error("TweenParticleEmitter", "freeing; free: " + getFree());
        }
    };
    private static Texture sparkle = new Texture("sparkle.png");

    static {
        Tween.registerAccessor(Sprite.class, new ParticleAccessor());
        Tween.setWaypointsLimit(20);
    }

    private float translateX = 0;
    private float translateY = 0;
    private final Array<Sprite> particles;

    private TweenManager tweenManager;
    private static final Vector2 vector2 = new Vector2();
    private int count;
    private Sound sound;

    private TweenParticleEmitter() {
        particles = new Array<Sprite>();
        tweenManager = new TweenManager();
    }

    public TweenParticleEmitter configure(Type type, int count, float duration, float x, float y, float angle, Color color, Sound sound) {
        this.count = count;
        this.sound = sound;

        setX(x);
        setY(y);
        setColor(color);

        translateX = 0;
        translateY = 0;

        switch (type) {
            case EXPLOSION:
                initExplosion(count, duration, x, y, angle);
                break;
            case SMOKE:
                initSmoke(count, duration, x, y, angle);
                break;
        }

        // the first time the actor is added (during an act loop)
        // the draw function is called once without the preceding act
        act(0);

        return this;
    }

    private void initSmoke(int count, float duration, float origx, float origy, float angle) {

        for (int i = 0; i < count; i++) {
            Sprite sprite;
            if (i >= particles.size) {
                sprite = new Sprite(sparkle);
                particles.add(sprite);
            } else {
                sprite = particles.get(i);
            }

            float r1 = RANDOM.nextFloat();
            float r2 = RANDOM.nextFloat();
            double r3Gaussian = RANDOM.nextGaussian();

            Tween t = getSmokeTween((i & 1) == 1 ? 1 : -1, duration, origx, origy, sprite, r1, r2, r3Gaussian);

            Timeline.createParallel()
                    .push(t)
                    .push(Timeline.createSequence()
                                    .push(Tween.to(sprite,
                                            ParticleAccessor.SCALE,
                                            t.getDuration() * 1.5f)
                                            //.ease(TweenEquations.easeOutQuad)
                                            .target(0.1f, 0.1f))
                    )
                    .repeat(Tween.INFINITY, 0)
                    .setUserData(sprite)
                    .start(tweenManager);
        }
    }

    private Tween getSmokeTween(int direction, float duration, float origx, float origy,
                                Sprite sprite, float r1, float r2, double r3Gaussian) {
        float x, y;
        float radius = 5 + r2 * 8;
        float scale = 1 + radius / 26;

        x = (float) (origx + r3Gaussian * 2f);
        y = origy + r1 * 5f;

        sprite.setBounds(x, y, radius, radius);
        sprite.setScale(scale, scale);

        direction = direction > 0 ? 1 : -1;
        float startingAngle = direction > 0 ? 0 : PI;

        Tween t = Tween.to(sprite,
                ParticleAccessor.XY,
                (float) Math.max(duration / 2, (duration + r3Gaussian / 3)))
                .delay(r1 / 3);

        Vector2 pointOfRotation = null;
        for (int i = 0; i < 3; i++) {
            pointOfRotation = getPointOfRotation(startingAngle + direction * (PI / 4) * i, radius, vector2);

            t.waypoint(x + pointOfRotation.x, y + pointOfRotation.y);
        }

        y = y + pointOfRotation.y + radius;

        radius *= .6666f;

        for (int i = 3; i < 4 + 3; i++) {
            pointOfRotation = getPointOfRotation(startingAngle - direction * (PI / 4) * i, radius, vector2);

            t.waypoint(x + pointOfRotation.x, y + pointOfRotation.y);
        }

        return t.target(x + pointOfRotation.x, y + pointOfRotation.y)
                .ease(TweenEquations.easeInOutSine)
                .path(TweenPaths.catmullRom);
    }

    private void initExplosion(int count, float duration, float x, float y, float angle) {
        double b1, b2;
        boolean directed = angle != Float.MIN_VALUE;
        if (directed) {
            b1 = (angle - PI / 2) % (2 * PI);
            b2 = b1 + PI;
        } else {
            b1 = 0;
            b2 = 2 * PI;
        }

        for (int i = 0; i < count; i++) {
            Sprite sprite;
            if (i >= particles.size) {
                sprite = new Sprite(sparkle);
                particles.add(sprite);
            } else {
                sprite = particles.get(i);
            }

            float r1 = RANDOM.nextFloat();
            float r2 = RANDOM.nextFloat();
            sprite.setBounds(x, y, 10 + 20 * r1, 8 + 16 * r1);
            sprite.setScale(1, 1);

            float angleRadians = (float) (b1 + r1 * (b2 - b1));
            Vector2 pointOfRotation = getPointOfRotation(angleRadians, r2 * 100, vector2);

            Tween.to(sprite, ParticleAccessor.XY, (float) Math.abs(duration + RANDOM.nextGaussian() * duration / 4))
                    .target(x + pointOfRotation.x,
                            y + pointOfRotation.y * (r1 <= .3 || directed ? 1 : -1))
                    .ease(TweenEquations.easeOutQuad)
                    .setUserData(sprite)
                    .start(tweenManager);
            Tween.to(sprite, ParticleAccessor.ALPHA, duration)
                    .target(0.3f)
                    .ease(TweenEquations.easeOutQuad)
                    .setUserData(sprite)
                    .start(tweenManager);
            Tween.to(sprite, ParticleAccessor.SCALE, duration)
                    .target(0.1f, 0.1f)
                            //.ease(TweenEquations.easeOutQuad)
                    .setUserData(sprite)
                    .start(tweenManager);

        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (tweenManager.getRunningTweensCount() == 0) {

            remove();

            return;
        }

        if (sound != null) {
            // TODO: take distance of player into account and set volume accordingly
            sound.play();
            sound = null;
        }

        tweenManager.update(delta);
    }

    @Override
    public boolean remove() {
        boolean removed = super.remove();

        if(removed) {
            tweenManager.killAll();

            actorPool.free(this);
        }

        return removed;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        List<BaseTween<?>> tweens = tweenManager.getObjects();
        int size = Math.min(count, tweens.size());

        for (int i = 0; i < size; i++) {
            BaseTween tween = tweens.get(i);
            if (tween.isFinished()) continue;

            Sprite sprite = (Sprite) tween.getUserData();

            batch.setColor(getColor());
            batch.draw(sprite.getTexture(),
                    sprite.getX() + translateX,
                    sprite.getY() + translateY,
                    sprite.getWidth() * sprite.getScaleX(),
                    sprite.getHeight() * sprite.getScaleY());
            batch.setColor(Color.WHITE);
        }
    }

    @Override
    public void setTranslation(float x, float y) {
        translateX += x;
        translateY += y;
    }

    public static Actor emitExplosion(int count, float duration, float x, float y, float angle, Color color, Sound sound) {
        return actorPool.obtain().configure(Type.EXPLOSION, count, duration, x, y, angle, color, sound);
    }

    public static Actor emitSmoke(int count, float duration, float x, float y, float angle, Color color, Sound sound) {
        return actorPool.obtain().configure(Type.SMOKE, count, duration, x, y, angle, color, sound);
    }

    public static void load() {
        assert actorPool != null;
    }

}
