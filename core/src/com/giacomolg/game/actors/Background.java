package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;

public class Background extends Group {

    private final Texture background;

    public Background(){
        background = new Texture("bgs.png");
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);

        if(parent == null){
            background.dispose();
        }
        else if(getChildren().size == 0) {
            addLayers();
        }
    }

    private void addLayers(){
        BackgroundLayer layerNear = new BackgroundLayer(background,
                1/14f, 1/3f, 0, background.getHeight() / 3 * 2, background.getWidth(), background.getHeight() / 3);

        BackgroundLayer layerFar = new BackgroundLayer(background,
                1/28f, 1/6f, 0, background.getHeight() / 3 * 1, background.getWidth(), background.getHeight() / 3);

        addActor(layerFar);
        addActor(layerNear);
    }
}
