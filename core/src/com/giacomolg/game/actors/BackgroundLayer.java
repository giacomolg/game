package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.viewport.Viewport;

public class BackgroundLayer extends Actor {

    private final Texture bg;
    private final float xParallax;
    private final float yParallax;
    private final int bgX;
    private final int bgY;
    private final int bgW;
    private final int bgH;
    private final TextureRegion[] regions;
    private int screenWidth;
    private int screenHeight;
    private float cameraXPos, cameraYPos;

    public BackgroundLayer(Texture bgTexture,
                           float xParallax, float yParallax, int bgX, int bgY, int bgW, int bgH) {
        this.bg = bgTexture;
        this.xParallax = xParallax;
        this.yParallax = 1; // TODO: yParallax;
        this.bgX = bgX;
        this.bgY = bgY;
        this.bgW = bgW;
        this.bgH = bgH;
        this.regions = new TextureRegion[]{
                new TextureRegion(bg),
                new TextureRegion(bg),
        };
    }

    private boolean updateBackgroundRegions() {
        Viewport viewport = getStage().getViewport();
        Camera camera = getStage().getCamera();
        screenWidth = viewport.getScreenWidth();
        screenHeight = viewport.getScreenHeight();

        float cameraYPos = getCameraYPos(camera);
        float cameraXPos = getCameraXPos(camera);

        TextureRegion region1, region2;
        if(cameraYPos <= bgH) {
            if (getX() == 0) {
                setX(cameraXPos);
                setY(cameraYPos);
                this.cameraXPos = cameraXPos;
                this.cameraYPos = cameraYPos;
            }

            if (this.cameraXPos != cameraXPos) {
                float x = getX() + (this.cameraXPos > cameraXPos ? -1 : 1) * Math.abs(this.cameraXPos - cameraXPos) * xParallax;
                setX(x);
                this.cameraXPos = cameraXPos;
            }
            if(this.cameraYPos != cameraYPos){
                float y = getY() + (this.cameraYPos > cameraYPos ? -1 : 1) * Math.abs(this.cameraYPos - cameraYPos) * yParallax;
                setY(y);
                this.cameraYPos = cameraYPos;
            }

            float xOffset = getX() % bgW;
            float yOffset = getY() % bgH;

            float invTexWidth = 1f / bg.getWidth();
            float invTexHeight = 1f / bg.getHeight();

            if (xOffset >= 0) {
                setTextureRegion(0, xOffset, bgY, bgW - xOffset, bgH - yOffset, invTexWidth, invTexHeight);
                setTextureRegion(1, 0, bgY, xOffset, bgH - yOffset, invTexWidth, invTexHeight);
            } else {
                setTextureRegion(0, bgW + xOffset, bgY, -xOffset, bgH - yOffset, invTexWidth, invTexHeight);
                setTextureRegion(1, 0, bgY, bgW + xOffset, bgH - yOffset, invTexWidth, invTexHeight);
            }

            return true;
        }

        return false;
    }

    private void setTextureRegion(int regionId, float x, float y, float width, float height,
                                           float invTexWidth, float invTexHeight){
        this.regions[regionId].setRegion(x * invTexWidth, y * invTexHeight,
                        (x + width) * invTexWidth, (y + height) * invTexHeight);
    }

    private float getCameraYPos(Camera camera) {
        return camera.position.y - (screenHeight >> 1);
    }

    private float getCameraXPos(Camera camera) {
        return camera.position.x - ( screenWidth >> 1);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(updateBackgroundRegions()) {
            TextureRegion region1 = regions[0], region2 = regions[1];

            float rationW = 1.5f * screenWidth / bgW;
            float rationY = 1.5f * screenHeight / bgH;
            float region1Width = region1.getRegionWidth() * rationW;
            batch.draw(region1, cameraXPos, cameraYPos, region1Width, region1.getRegionHeight() * rationY);
            batch.draw(region2, cameraXPos + region1Width, cameraYPos, region2.getRegionWidth() * rationW, region2.getRegionHeight() * rationY);
        }
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);

        if(parent == null){
            bg.dispose();
        }
    }
}
