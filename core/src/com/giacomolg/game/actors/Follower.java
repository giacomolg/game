package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Follower extends Actor {

    private final Actor follower;
    private final Actor toBeFollowed;
    float prevX;
    float prevY;

    private Follower(Actor follower, Actor toBeFollowed) {
        this.follower = follower;
        this.toBeFollowed = toBeFollowed;
        prevX = toBeFollowed.getX();
        prevY = toBeFollowed.getY();
    }

    @Override
    public void act(float delta) {
        float currentX = toBeFollowed.getX();
        float currentY = toBeFollowed.getY();

        //Gdx.app.error("Follower", currentX + ", " +currentY);

        float xDiff = currentX - prevX;
        float yDiff = currentY - prevY;
        boolean changedX = ! MathUtils.isZero(xDiff);
        boolean changedY = ! MathUtils.isZero(yDiff);
        float translateX = 0, translateY = 0;

        if(changedX){
            translateX = xDiff * .15f;
            prevX += translateX;
        }

        if(changedY){
            translateY = yDiff * .15f;
            prevY += translateY;
        }

        if(changedX || changedY) {
            //Gdx.app.error(""+translateX, ", " + translateY);
            ((Translatable) follower).setTranslation(translateX, translateY);
        }

        follower.act(delta);
    }

    @Override
    public float getX() {
        return prevX;
    }

    @Override
    public float getY() {
        return prevY;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        follower.draw(batch, parentAlpha);
    }

    public static Follower makeFollowerOf(Actor follower, Actor toBeFollowed) {
        if(!(follower instanceof Translatable))
            throw  new IllegalArgumentException("Actor must be Translatable");

        return new Follower(follower, toBeFollowed);
    }
}
