package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.ReflectionPool;


public class Timed extends Group implements Pool.Poolable{

    private static ReflectionPool<Timed> pool = new ReflectionPool<Timed>(Timed.class, 30, 60);
    private float time;
    Actor actor;

    private Timed(){  }

    public void setTime(float time) {
        this.time = time;
    }

    @Override
    public void act(float delta) {

        if(time <= 0) {
            remove();

            return;
        }

        time -= delta;

        // actor.act(delta);
        super.act(delta);
    }

//    @Override
//    public void draw(Batch batch, float parentAlpha) {
//        actor.draw(batch, parentAlpha);
//    }

    @Override
    public boolean remove() {
        boolean removed = super.remove();

        if(removed){
            for(Actor c : getChildren())
                c.remove();

            pool.free(this);
        }

        return removed;
    }

    @Override
    public void reset() {
        clearChildren();
    }

    public static Timed create(Actor actor, float time){
        Timed timed = pool.obtain();

        timed.addActor(actor);
        // timed.actor = actor;
        timed.time =time;

        return timed;
    }
}
