package com.giacomolg.game.actors;

public interface Translatable {

    void setTranslation(float x, float y);
}
