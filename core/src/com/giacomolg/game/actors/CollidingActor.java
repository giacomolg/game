package com.giacomolg.game.actors;

import com.badlogic.gdx.physics.box2d.ContactListener;

public interface CollidingActor extends ContactListener { }
