package com.giacomolg.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.giacomolg.game.Game;

public class HUD extends Actor {

    private final Viewport viewport;
    private final BitmapFont font = new BitmapFont();
    private final StringBuilder sb = new StringBuilder();

    public HUD(Viewport viewport){
        this.viewport = viewport;
    }

    @Override
    public void act(float delta) {
        Camera camera = viewport.getCamera();
        Vector3 pos = camera.position;
        float x = pos.x - viewport.getScreenWidth() / 2f + 10;
        float y = pos.y - viewport.getScreenHeight() / 2f + 20;

        setX(x);
        setY(y);

        sb.delete(0, sb.length());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.begin();
        font.draw(batch,
                sb.append("FPS: ").append(Gdx.graphics.getFramesPerSecond()).append("(").append(Game.millisPerFrame).append(")"),
                getX(), getY());
        batch.end();
    }

}
