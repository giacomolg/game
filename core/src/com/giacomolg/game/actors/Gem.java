package com.giacomolg.game.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.giacomolg.game.actors.particles.TweenParticleEmitter;
import com.giacomolg.game.actors.player.Player;
import com.giacomolg.game.resources.Assets;
import com.giacomolg.game.utils.B2DValues;

import static com.giacomolg.game.stages.GameStage.SHAPE_RENDERER;
import static com.giacomolg.game.utils.B2DValues.toB2D;
import static com.giacomolg.game.utils.Math.getPointOfRotation;
import static java.lang.Math.atan2;

public class Gem extends B2DActor {
    private static final int FORCE_JUMP = 250;
    private static FixtureDef fdef = new FixtureDef();
    private static Shape shape = new CircleShape();
    static {
        fdef.isSensor = true;
        shape.setRadius(toB2D(7.5f));
    }

    private final Player player;

    public Gem(World world, Player player, Vector2 pos){
        this(world, player, pos, Color.YELLOW);
    }

    public Gem(World world, Player player, Vector2 pos, Color color) {
        super(world);
        this.player = player;

        setColor(color);

        setX(pos.x);
        setY(pos.y);
        setWidth(15);
        setHeight(15);

        initB2D(BodyDef.BodyType.StaticBody, shape, fdef);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        SHAPE_RENDERER.setProjectionMatrix(batch.getProjectionMatrix());
        SHAPE_RENDERER.setTransformMatrix(batch.getTransformMatrix());

        SHAPE_RENDERER.begin(ShapeRenderer.ShapeType.Filled);
        SHAPE_RENDERER.setColor(getColor());
        SHAPE_RENDERER.ellipse(getX(), getY(), getWidth(), getHeight());
        SHAPE_RENDERER.end();

        batch.begin();
    }


    @Override
    public void endContact(Contact contact) {
        super.endContact(contact);
    }

    @Override
    public void beginContact(Contact contact) {
        super.beginContact(contact);

        Fixture collidingFixture = getCollidingFixture(contact);
        if(collidingFixture.getFilterData().categoryBits == B2DValues.BULLET_BIT){
            Bullet bullet = (Bullet) collidingFixture.getUserData();


            if(! bullet.hasPowerLeft()) return;

            getStage().addActor(Timed.create(TweenParticleEmitter.emitSmoke(
                    2,
                    2f,
                    getX(),
                    getY(),
                    Float.MAX_VALUE,
                    Color.ORANGE, null),
                    5 * 2f));

            Body body = player.getBody();
            Vector2 linearVelocity = body.getLinearVelocity();
            if(linearVelocity.y < 0) body.setLinearVelocity(linearVelocity.x, linearVelocity.y * .8f);

            float opposite = player.getY() + player.getScaledHeight() / 2 - getY() + getHeight() / 2;
            float adjacent = player.getX() + player.getScaledWidth() / 2 - getX() + getWidth() / 2;
            double distance = Math.sqrt(opposite * opposite + adjacent * adjacent);
            double angle = atan2(opposite, adjacent);

            Vector2 pointOfRotation = getPointOfRotation((float)angle, 1f);

            getStage().addActor(TweenParticleEmitter.emitExplosion(
                    200,
                    .5f,
                    getX(),
                    getY(),
                    (float) angle,
                    Color.RED,
                    Assets.BLOOD_SPLATTER));

            if(Math.abs(linearVelocity.y) < 20) {
                double f = 200 / distance;
                double xForce = f * FORCE_JUMP * 1 * pointOfRotation.x;
                double yForce = f * FORCE_JUMP * 3 * pointOfRotation.y;

                body.applyForceToCenter((float) xForce, (float) yForce, true);
            }

            // remove();
        }
    }

}
