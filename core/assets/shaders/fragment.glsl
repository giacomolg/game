#ifdef GL_ES
#define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;
uniform vec2 u_centre;
uniform vec2 u_resolution;

void main()
{
   vec4 color;

   if(u_time > 0.0){
       //vec2 iResolution = vec2(640.0, 480.0);
       /*vec2 uv = gl_FragCoord.xy / iResolution.xy;
       vec2 c = u_centre.xy / iResolution.xy;

       float w = (0.5 - (uv.x)) * (iResolution.x / iResolution.y);
       float h = 0.5 - uv.y;
       vec2 dv = vec2(w,h);
       float distanceFromCenter = sqrt(dot(dv,dv));


       float sinArg = distanceFromCenter * 10.0 - u_time * 10.0;
       float slope = cos(sinArg) ;
       color = v_color * texture2D(u_texture, uv + normalize(vec2(w, h)) * slope * 0.01);*/

       const float speed = 10.0;
       const float distortionAmount = 0.005;
       const float distortionIntensity = 1.0;

       vec2 uv = (gl_FragCoord.xy+v_texCoords.xy) / u_resolution.xy;
       vec2 relativeMouse = u_centre.xy / u_resolution.xy;

       vec2 dv = vec2(relativeMouse.x - uv.x, (1f - relativeMouse.y) - uv.y);
       float slope = cos(length(dv) * distortionIntensity - u_time * speed);

       color = texture2D(u_texture, /*(vec2(uv.x, 1f-uv.y))*/v_texCoords + /*normalize(dv)*/dv * slope * distortionAmount);
    }
    else{
        color = v_color * texture2D(u_texture, v_texCoords);
    }
   gl_FragColor = color;
}